#encoding: utf-8

require "epimath100"
require 'gruff'
require 'myerror'
require_relative 'interactive'

def list_to_txt xlist, ylist, name
  f = File.open(name.to_s, "w")
  size = [xlist.size, ylist.size].max
  size.times do |i|
    f << "#{xlist[i]};#{ylist[i]}\n"
  end
end

def splitter str
  str.to_s.split('|').collect{|e| e.to_i}
end

def main_get_poly str
  return EpiMath::Polynomial.new(splitter(str))
end

def main_get_poly_list argv
  l_poly = []
  i = 0
  while i < argv.size do
    l_poly << [main_get_poly(argv[i]),
               main_get_poly(argv[i+1])]
    i += 2
  end
  return l_poly
end

def main_get_ratio_list l_poly
  l_ratio = []
  l_poly.each do |p, q|
    l_ratio << EpiMath::Rational.new(p, q)
  end
  return l_ratio
end

def main_get_gruff size
  g = Gruff::Bezier.new '1200x800'
  g.title = 'Graph of the Death'
  g.labels = {
    0 => '-10',
    (size*1/4 - 1) => '-5',
    (size*2/4 - 1) => '0',
    (size*3/4 - 1) => '5',
    (size*4/4 - 1) => '10'
  }
  return g
end

def main_get_xlist
  xl = []
  i = -10.0
  while i <= 10.0
    xl << i
    i = (i + 0.01).round(2)
  end
  return xl
end

def main_get_ylist xl, f
  yl = []
  xl.each do |x|
    yl << f.calc(x).round(2)
  end
  return yl
end

def max_from_a_a a
  m = []
  a.each do |x|
    m << x.max
  end
  return m.max
end

def option1_naif poly
  time = Time.new
  x = 0.0

  while x < 1000
    poly.calc x
    x += 0.001
  end

  return Time.now - time
end

def option1_horner poly
  time = Time.new
  x = 0.0

  i = poly.get_degree_max - 1
  while x < 1000
    p = poly.get_degree(poly.get_degree_max)
    while i > 0
      p = poly.get_degree i + x * p
      i -= 1
    end
    x += 0.001
  end

  return Time.now - time
end

def main_option1(argv)
  poly = main_get_poly(argv)
  t1 = option1_naif(poly)
  t2 = option1_horner(poly)
  r = (t1.to_f / t2.to_f).round(3)
  puts t1.round(3).to_s + " secondes via la methode naive"
  puts t2.round(3).to_s + " secondes via la methode Horner"
  puts "Soit un rapport de #{r} de la vitesse de la methode Horner\
 par rapport à la methode rapide"
  return
end

def main_option2(argv)
  xl = main_get_xlist
  g = main_get_gruff xl.size
  puts xl.size
  fl = main_get_ratio_list(main_get_poly_list(argv))
  yl = []
  fl.size.times do |i|
    f = fl[i]
    puts f
    yl << main_get_ylist(xl, f)
    g.data "function(#{i})".to_sym, yl[-1]
  end

  g.write("out.png")
  `eog out.png`
end

#get the polynome list from arguments and make rational function from them
#then put it in the terminal and in the graph
# TODO :
# -time
# -better /0
# -bigger lines
def main argv
  if argv.size == 0
    interactive()
  elsif argv.size % 2 == 1
    main_option1(argv)
  else
    main_option2(argv)
  end
end
